from keras.layers import Dense, Embedding, LSTM, TimeDistributed, Input, Bidirectional, Dropout
from keras.models import Model
from keras_contrib.layers import CRF


def create_model(maxlen, chars, word_size, infer=False):
    """

    :param infer:
    :param maxlen:
    :param chars:
    :param word_size:
    :return:
    """
    sequence = Input(shape=(maxlen,), dtype='int32')
    embedded = Embedding(len(chars) + 1, word_size, input_length=maxlen, mask_zero=True)(sequence)
    blstm1 = Bidirectional(LSTM(64, return_sequences=True), merge_mode='sum')(embedded)
    blstm2 = Bidirectional(LSTM(64, return_sequences=True), merge_mode='sum')(blstm1)
    output = TimeDistributed(Dense(5, activation='softmax'))(blstm2)
    model = Model(inputs=sequence, outputs=output)
    if not infer:
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

